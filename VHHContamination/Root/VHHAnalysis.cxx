#include <AsgTools/MessageCheck.h>
#include <VHHContamination/VHHAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>

#include <iostream>

#include <xAODBase/IParticle.h>
#include <xAODRootAccess/TActiveStore.h>
#include <xAODCore/AuxContainerBase.h>
#include <xAODRootAccess/TStore.h>

VHHAnalysis :: VHHAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
}

VHHAnalysis :: ~VHHAnalysis () {
  delete m_pdgId;
  delete m_prodVtx;
}

StatusCode VHHAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));
  TTree* mytree = tree ("analysis");
  mytree->Branch ("RunNumber", &m_runNumber);
  mytree->Branch ("EventNumber", &m_eventNumber);

  m_pdgId = new std::vector<float>();
  mytree->Branch ("truth_pdgId", &m_pdgId);
  m_prodVtx = new std::vector<float>();
  mytree->Branch ("truth_prodVtx_barcode", &m_prodVtx);

  nVBF = 0;
  nVHH = 0;
  return StatusCode::SUCCESS;
}

StatusCode VHHAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  //  ANA_MSG_INFO ("in execute");

  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  m_pdgId->clear();
  m_prodVtx->clear();

  const xAOD::TruthParticleContainer* truthPart = nullptr;
  ANA_CHECK (evtStore()->retrieve (truthPart, "TruthParticles"));
  int nHiggs = 0;
  int nWZ = 0;
  int nQ = 0;

  for (const auto tp: *truthPart){
    const ElementLink<xAOD::TruthVertexContainer> prodVtxLink = tp->auxdata<ElementLink< xAOD::TruthVertexContainer > > ("prodVtxLink");
    if (prodVtxLink.isValid()) {
      const xAOD::TruthVertex* tvtx = *prodVtxLink;
      m_pdgId->push_back (tp->pdgId());
      m_prodVtx->push_back (tvtx->barcode());

      if (tvtx->barcode()==-1 && tp->pdgId()!=2212){
	if (tp->pdgId()==25) nHiggs++;
	if(abs(tp->pdgId())==23 || abs(tp->pdgId())==24) nWZ++;
	if(abs(tp->pdgId())==1 || abs(tp->pdgId())==2 || abs(tp->pdgId())==3 || abs(tp->pdgId())==4 || abs(tp->pdgId())==5 || abs(tp->pdgId())==6) {
	  nQ++;
	}
      }
    }
  }

  if(nHiggs==2 && nQ==2 && nWZ==0) nVBF++;
  if(nHiggs==2 && nQ==0 && nWZ==1) nVHH++;

  // Fill the event into the tree:
  tree ("analysis")->Fill ();

  return StatusCode::SUCCESS;
}


StatusCode VHHAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  std::cout << "=================================================" << "\n" 
	    << "PRINTING Number of VBF and VHH Events" << "\n"
	    << "VBF " << nVBF << " VHH " << nVHH << "\n"    
	    << "=================================================" << "\n"
            << "PRINTING Number of VBF and VHH Fractions" << "\n"
            << "VBF " << nVBF/(nVBF+nVHH) << " VHH " << nVHH/(nVBF+nVHH) << "\n"
	    << "=================================================" << std::endl;

  return StatusCode::SUCCESS;
}
