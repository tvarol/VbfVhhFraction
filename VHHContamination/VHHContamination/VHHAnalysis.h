#ifndef VHHContamination_VHHAnalysis_H
#define VHHContamination_VHHAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TH1.h>
#include <xAODJet/JetContainer.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthVertexContainer.h>
#include <TTree.h>
#include <vector>

class VHHAnalysis : public EL::AnaAlgorithm
{
 public:
  // this is a standard algorithm constructor
  VHHAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
  ~VHHAnalysis () override;

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

 private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;

  float nVBF;
  float nVHH;

  /// output variables for the current event
  /// \{
  unsigned int m_runNumber = 0; ///< Run number
  unsigned long long m_eventNumber = 0; ///< Event number
  /// Jet 4-momentum variables
  //  std::vector<float> *m_jetEta = nullptr;
  //std::vector<float> *m_jetPhi = nullptr;
  //std::vector<float> *m_jetPt = nullptr;
  //std::vector<float> *m_jetE = nullptr;
  /// \}
  std::vector<float> *m_pdgId = nullptr;
  std::vector<float> *m_prodVtx = nullptr;

};

#endif
