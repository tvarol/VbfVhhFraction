# Author/Contact
Tulin Varol Mete  Tulin.Varol@cern.ch

# Download, Setup and Compilation

```	   
mkdir myWorkArea
cd myWorkArea
git clone ssh://git@gitlab.cern.ch:7999/tvarol/VbfVhhFraction.git
mkdir run build
cd build
setupATLAS
asetup 21.2.114,AnalysisBase
cmake ../VbfVhhFraction/
cmake --build .
source x86_64-centos7-gcc8-opt/setup.sh
```
# Running the Code
```
cd ../run
export pathToDAOD=PATH_TO_YOUR_DAODS
runVHHAnalysis_eljob.py --submission-dir=submitDir
```
